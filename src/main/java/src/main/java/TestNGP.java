package src.main.java;

import org.testng.annotations.Test;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.GooglePage;
import org.testng.annotations.Parameters;


 
public class TestNGP {

public  WebDriver driver;

@Parameters({"browserType"})
@Test
   public void tC1(String browserType) throws IOException, InterruptedException,MalformedURLException{
	DesiredCapabilities capability=null;
    if(browserType.equals("firefox"))
    {
    System.setProperty("webdriver.gecko.driver", "/selenium/geckodriver");
    capability=DesiredCapabilities.firefox();
    capability.setCapability("marionette",true);
    capability.setBrowserName("firefox");
    capability.setPlatform(Platform.LINUX);
 
    }else{
    	System.setProperty("webdriver.chrome.driver", "/selenium/chromedriver");
    	capability=DesiredCapabilities.chrome();
    	capability.setBrowserName("chrome");
    	capability.setPlatform(Platform.LINUX);
    }
               
                //ChromeOptions chromeOptions = new ChromeOptions();
                //chromeOptions.addArguments("--headless");
                //chromeOptions.addArguments("--no-sandbox");
                String hubURL = "http://localhost:4444"; 
               
                
                driver = new RemoteWebDriver(new URL(hubURL), capability); 
                GooglePage gPage=new GooglePage(driver);
                ExtentReport reports = new ExtentReport();
                reports.startTest();
 
                driver.get("https://google.com");
 
                Thread.sleep(10000);
 
                if (gPage.getTitle().contains("Gmail")) {
                        System.out.println("Pass");
                        reports.logStatusPass(driver);
                } else {
                        System.out.println("Fail");
                        reports.logStatusFail(driver);
                }
                reports.endTest();
                driver.quit();
               
   }
        
}
