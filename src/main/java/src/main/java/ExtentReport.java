package src.main.java;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReport {
public ExtentReports report;
public ExtentTest test;

public void startTest()
{
report = new ExtentReports(System.getProperty("user.dir")+"/Selenium/"+"ExtentReportResults.html");
test   = report.startTest("ExtentReport");
}


public String capture(WebDriver driver) throws IOException {
File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
File dest = new File("/home/adarsh/Downloads/Selenium/Selenium/ScreenShot" + System.currentTimeMillis() + ".png");
String errflpath = dest.getAbsolutePath();
FileUtils.copyFile(scrFile, dest);
return errflpath;
}

public void logStatusPass(WebDriver driver) throws IOException
{
test.log(LogStatus.PASS,test.addScreenCapture(capture(driver)),"Navigated to the specified URL");
}

public void logStatusFail(WebDriver driver) throws IOException
{
test.log(LogStatus.FAIL,test.addScreenCapture(capture(driver)), "Test Failed");
}

public void endTest()
{
report.endTest(test);
report.flush();
}


}