package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GooglePage {
	
WebDriver driver;

@FindBy(className="gb_t")
WebElement gmail;

public GooglePage(WebDriver driver)
{
	this.driver=driver;
	PageFactory.initElements(driver, this);
}

public String getTitle()
{
	return gmail.getText();
}

}
